(function(){
    'use strict';

    var maria = [{"mleft":76,"width":133,"text":{"value":"Budget control"},"opts":{"dayhelper":"20"},"milestone":{"right":null,"dayhelper":null,"line":{"width":null}}},{"mleft":319,"width":132,"text":{"value":"Budget control"},"opts":{"dayhelper":"20"},"milestone":{"right":null,"dayhelper":null,"line":{"width":null}}},{"mleft":559,"width":132,"text":{"value":"Budget control"},"opts":{"dayhelper":"20"},"milestone":{"right":null,"dayhelper":null,"line":{"width":null}}},{"mleft":799,"width":132,"text":{"value":"Budget control"},"opts":{"dayhelper":"20"},"milestone":{"right":null,"dayhelper":null,"line":{"width":null}}},{"mleft":-58,"width":298,"text":{"value":"balance, report and audited annual accounts"},"opts":{"dayhelper":"30"},"milestone":{"right":-85,"dayhelper":"30","line":{"width":66}}},{"mleft":136,"width":222,"text":{"value":"budget and activity plan review"},"opts":{"dayhelper":"15"},"milestone":{"right":-91,"dayhelper":"15","line":{"width":72}}},{"mleft":136,"width":222,"text":{"value":"policies analysis and review"},"opts":{"dayhelper":"15"},"milestone":{"right":-93,"dayhelper":"15","line":{"width":74}}},{"mleft":107,"width":250,"text":{"value":"staff and salaries analysis and review"},"opts":{"dayhelper":"15"},"milestone":{"right":-92,"dayhelper":"15","line":{"width":73}}},{"mleft":572,"width":229,"text":{"value":"annual budget and activity plans"},"opts":{"dayhelper":"1"},"milestone":{"right":-47,"dayhelper":"15","line":{"width":28}}}];

    function Task(mleft, width, textValue, optsDay, mRight, mDay, mLineWidth) {
        this.mleft = mleft;
        this.width = width;
        this.text = { 
            value: textValue
        };
        this.opts = {
            dayhelper: optsDay
        };
        this.milestone = {
            right: mRight,
            dayhelper: mDay,
            line: {
                width: mLineWidth
            }
        }
    }

    function parseDayHelper(DOMobject) {
        if (DOMobject.length == 0) { return null; }
        return DOMobject[0].innerHTML;
    }

    function parseCSSvalue(CSSvalue) {
        if (CSSvalue === undefined) { return null; }
        return parseInt(CSSvalue);
    }

    function exportTasksAsObject(){

        var calendarObj = [];

        $(".task-rows .row").each(function(index) {

            var mleft      = parseCSSvalue($(this).find(".task").css("margin-left"));
            var width      = parseCSSvalue($(this).find(".task").css("width"));
            var textValue  = $(this).find(".task-text input")[0].value;
            var optsDay    = parseDayHelper($(this).find(".task-opts .day-helper"));
            var mRight     = parseCSSvalue($(this).find(".milestone").css("right"));
            var mDay       = parseDayHelper($(this).find(".milestone .day-helper"));
            var mLineWidth = parseCSSvalue($(this).find(".milestone .milestone-line svg").css("width"));

            var task = new Task(mleft, width, textValue, optsDay, mRight, mDay, mLineWidth);

            calendarObj.push(task);
        });

        return calendarObj;
    }

    // var exported = exportTasksAsObject();
    // console.log(JSON.stringify(exported));


    function loadFromJSON(jsonString) {

        $(".task-rows").empty();

        maria.forEach(function(element, index){

            $(".task-rows").append(Handlebars.templates.taskline());

            var thisRow = $(".task-rows .row")[index];

            $(thisRow).find(".task").css("margin-left", element.mleft);
            $(thisRow).find(".task").css("width", element.width);
            $(thisRow).find(".task-text input")[0].value = element.text.value;
            $(thisRow).find(".task-opts .day-helper").innerHTML = element.opts.dayhelper;
            $(thisRow).find(".milestone").css("right", element.milestone.right);
            $(thisRow).find(".milestone .day-helper").innerHTML = element.milestone.dayhelper;
            $(thisRow).find(".milestone .milestone-line svg").css("width", element.milestone.line.width);


        });

    }

    // loadFromJSON();


}())