(function(){
    'use strict';

    
    // GLOBAL VARS

    var mouseX           = 0;
    var minMilestoneLeft = -19;

    
    // FUNCTIONS

    function replaceIcons() {

        feather.replace({height: "16px"});
    }

    function addTask() {

        $(".task-rows").append(Handlebars.templates.taskline());
        replaceIcons();
    }

    function saveTaskList() {

        var taskList = $(".task-rows")[0].innerHTML;
        localStorage.setItem("tasklist", taskList);
    }

    function restoreTaskList() {

        var taskList = localStorage.getItem("tasklist");
        
        if (taskList == null) {
            addTask();
        } else {
            $(".task-rows")[0].innerHTML = taskList;
            replaceIcons();
        }
    }

    
    // SITEWIDE EVENTS

    $(document).on("mousemove", function(event){ mouseX = event.pageX; });
    
    $(document).on("mouseleave", ".task-rows", function() {

        $(".drag svg").toggle(false);
        $(".task-opts .opts").toggle(false);
    });

    
    // MOVEMENT

    // function dragRightSide()
    $(".task-rows").on("mousedown", ".drag-right", function() {

        $(".task-rows").sortable("disable");

        var isDragging = 0;

        var slider = $(this).parent();
        
        var startCalendarW = $(".calendar").width();
        var startSliderW = $(slider).width();
        var startMouseX = mouseX;

        isDragging = 1;

        $(".container").on("mousemove", function() {

            if (!isDragging) return;

            // calc percentage
            var mouseOffset = mouseX - startMouseX;
            var destSliderW = (100 * (startSliderW + mouseOffset) / startCalendarW);
            
            $(slider).width(destSliderW+"%");

            // update day helper;            
            var calOffset = $(".calendar").offset().left;
            var sliderOffset = $(slider).offset().left;
            var currSliderW = $(slider).width();            

            var milestonePos = (100 * ((sliderOffset + currSliderW) - calOffset) / startCalendarW) / 8.33333;
            var month = parseInt(milestonePos);
            var day = parseInt((milestonePos - month) * 1000 / 31);

            console.log(sliderOffset, milestonePos, month, day);

            $(slider).find(".day-helper")[0].innerHTML = day;
        });

        $(document).on("mouseup", function() {
            isDragging = 0;
            $(".task-rows").sortable("enable");
        });        
    });

    // function dragLeftSide()
    $(".task-rows").on("mousedown", ".drag-left", function(){
        $(".task-rows").sortable("disable");

        var isDragging = 0;

        var slider = $(this).parent();

        var startCalendarW = $(".calendar").width();
        var startSliderW = $(slider).width();
        var startSliderMargin = $(slider).outerWidth(true);
        var startMouseX = mouseX;

        isDragging = 1;

        $(".container").on("mousemove", function(){

            if (!isDragging) return;

            // calc percentage
            var mouseOffset = mouseX - startMouseX;
            var sliderOffset = startSliderW - mouseOffset;
            var sliderMarginOffset = startSliderMargin - startSliderW + mouseOffset;
            var destSliderW = (100 * sliderOffset / startCalendarW);
            var destSliderMargin = (100 * sliderMarginOffset / startCalendarW);
            
            $(slider).width(destSliderW+"%");
            $(slider).css("margin-left", destSliderMargin+"%");
        });

        $(document).on("mouseup" , function() {
            isDragging = 0;
            $(".task-rows").sortable("enable");
        });
    });

    // function dragTask()
    // Drag vertically
    $(".task-rows").sortable({
        axis: "y",
        containment: "parent",
        cursor: "move",
        items: "> .row",
        cancel: ".milestone,.drag",
    });

    // Drag horizontally
    $(".task-rows").on("mousedown", ".task-text", function(){
        var isDragging = 0;

        var slider = $(this).parent();
        
        var startCalendarW = $(".calendar").width();
        var startSliderW = $(slider).width();
        var startSliderMargin = $(slider).outerWidth(true);
        var startMouseX = mouseX;

        isDragging = 1;

        $(".container").on("mousemove", function(){

            if (!isDragging) return;

            var mouseOffset = mouseX - startMouseX;
            var sliderMarginOffset = startSliderMargin - startSliderW + mouseOffset;
            var destSliderMargin = (100 * sliderMarginOffset / startCalendarW);

            $(slider).css("margin-left", destSliderMargin+"%");

            // Movement limits needed!

        });

        $(document).on("mouseup" , function() {
            isDragging = 0;
        });
    });

    // function dragMilestone()
    $(".task-rows").on("mousedown", ".milestone", function() {
        var isDragging = 0;

        var milestone = $(this);
        var milestoneLine = $(this).find(".milestone-line svg");

        var startMilestonePos = parseInt($(milestone).css("right"));
        var startLineW = parseInt($(milestoneLine).css("width"));
        var startMouseX = mouseX;

        isDragging = 1;

        $(".container").on("mousemove", function(){

            if (!isDragging) return;

            var mouseOffset = mouseX - startMouseX;
            var milestoneOffset = startMilestonePos - mouseOffset;
            var milestoneLineOffset = startLineW + mouseOffset;

            // Limit left movement;
            if (milestoneOffset >= minMilestoneLeft) milestoneOffset = minMilestoneLeft;

            $(milestone).css("right", milestoneOffset+"px");
            $(milestoneLine).css("width", milestoneLineOffset+"px");

            // update day helper;
            var startCalendarW = $(".calendar").width();
            var calOffset = $(".calendar").offset().left;            

            var milestonePos = (100 * (mouseX - calOffset) / startCalendarW) / 8.33333;
            var month = parseInt(milestonePos);
            var day = parseInt((milestonePos - month) * 1000 / 31);

            $(milestone).find(".day-helper")[0].innerHTML = day;

        });

        $(document).on("mouseup" , function() {
            isDragging = 0;
        });
    });

    
    // TASK OPTIONS

    // function enableEditableText()
    $(".task-rows").on("focusout", ".task-text input", function(){
        $(this).attr("value", $(this)[0].value);
    });

    // force text to be editable
    $(".task-rows").on("click", ".row", function() {
        $(this).find(".task-text input").focus();
    });

    // function showTaskOptions()
    $(".task-rows").on("mouseenter mouseleave", ".row", function(){
        $(this).find(".drag svg").toggle();
        $(this).find(".task-opts .opts").toggle();
    });

    // $(".task-rows").on("mouseenter mouseleave", ".row", function() {
    //     $(this).toggleClass("mtop-20");
    // });


    // OPTION BUTTONS

    // function addMilestone()
    $(".task-rows").on("click", ".add-milestone", function() {
        $(this).closest(".task").append(Handlebars.templates.milestone());
        replaceIcons();
    });

    // remove milestone
    $(".task-rows").on("contextmenu", ".milestone", function() {
        $(this).remove();
    });

    // remove task
    $(".task-rows").on("click", ".del-task", function() {
        $(this).closest(".row").remove();
    });

    // clone task
    $(".task-rows").on("click", ".clone-task", function() {
        $(this).closest(".row").clone().appendTo(".task-rows");
    });


    // BUTTONS

    // function btnAddTask() 
    $(".btn-add-task").on("click", function(){ addTask(); });

    // function btnSaveTasks()
    $(".btn-save-tasks").on("click", function(){ saveTaskList(); });

    // function btnRestoreTasks()
    $(".btn-restore-tasks").on("click", function(){ restoreTaskList(); });

    // function btnClearTasks()
    $(".btn-clear-tasks").on("click", function(){
        $(".task-rows").empty();
        localStorage.clear();
    });


    // SITE INIT

    restoreTaskList();

}())