# the board gantt

a very simple but powerfull macro task manager

## technologies

* html 5
* css 3
* jquery 2
* handlebars 4
* feather
* localStorage

## features

* create new tasks, set the starting point and duration
* set extra milestones for each task
* move tasks around freely
* save your calendar on your browser's local storage

## future features

* fewer bugs
* save/load calender to/from disk/server
* year scroll
* task dependencies